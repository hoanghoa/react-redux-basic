import {
  TODO_LIST_REQUEST,
  TODO_LIST_SUCCESS,
  TODO_LIST_FAIL,
} from "../constants/todoConstants";

export const todoListReducers = (
  state = { loading: true, todos: [] },
  action
) => {
  switch (action.type) {
    case TODO_LIST_REQUEST:
      return { loading: true };
    case TODO_LIST_SUCCESS:
      return { loading: false, todos: action.payload };
    case TODO_LIST_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
