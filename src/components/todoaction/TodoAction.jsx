import React from "react";
import Button from "../button/Button";
import "./todo-action.css";

const TodoAction = (props) => {
  const currentTodo = props.current;
  return (
    <div className="todo__action">
      <div className="buttons">
        <Button color="#5E5DF0" onClick={() => console.log(currentTodo.id)}>
          Share with
        </Button>
        <Button color="#DC4C3F" onClick={() => console.log(currentTodo.id)}>
          Delete
        </Button>
      </div>
    </div>
  );
};

export default TodoAction;
