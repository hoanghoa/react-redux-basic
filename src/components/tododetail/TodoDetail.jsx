import React from "react";
import "./todo-detail.css";
const TodoDetail = (props) => {
  const todo = props.current;

  return (
    <div id="todo-detail" className="todo__detail">
      <div className="todo__heading">
        <h1>{todo.title}</h1>
      </div>

      <p>{todo.title}</p>
      <span>Status: </span>
      {todo.completed ? (
        <>
          <ion-icon name="checkmark-done-outline"></ion-icon>
          <span>Complete</span>
        </>
      ) : (
        <>
          <ion-icon name="ellipsis-horizontal-outline"></ion-icon>
          <span>Working</span>
        </>
      )}
    </div>
  );
};

export default TodoDetail;
