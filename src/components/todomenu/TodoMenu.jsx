import React from "react";
import useContextMenu from "../../hooks/useContextMenu";
import "./todo-menu.css";

const TodoMenu = () => {
  const { anchorPoint, show } = useContextMenu();

  if (show)
    return (
      <div className="menu" style={{ top: anchorPoint.y, left: anchorPoint.x }}>
        <div className="menu__item">
          Edit todo
          <div className="menu__hotkey">Shift + E</div>
        </div>
        <div className="menu__item">
          Pin todo
          <div className="menu__hotkey">Shift + P</div>
        </div>
        <div className="menu__divider"></div>
        <div className="menu__item">
          Share todo
          <div className="menu__hotkey">Shift + S</div>
        </div>
        <div className="menu__item">
          Delete todo
          <div className="menu__hotkey">Shift + D</div>
        </div>
      </div>
    );
  return <></>;
};

export default TodoMenu;
