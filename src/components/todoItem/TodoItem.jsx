import React from "react";
import PropTypes from "prop-types";
import "./todo-item.css";

const TodoItem = (props) => {
  return (
    <div className="todo__item" onClick={props.onClick}>
      <ion-icon name="calendar-clear-outline"></ion-icon>
      <span className="truncate">{props.todo.title}</span>
    </div>
  );
};

TodoItem.propTypes = {
  todo: PropTypes.object,
  onClick: PropTypes.func,
};

export default TodoItem;
