import React from "react";
import PropTypes from "prop-types";

import "./button.css";

const Button = (props) => {
  return (
    <button
      className="button"
      style={{
        backgroundColor: props.color,
      }}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  color: PropTypes.string.isRequired,
};

export default Button;
