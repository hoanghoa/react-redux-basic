import React from "react";
import PropTypes from "prop-types";
import "./header.css";

const Header = (props) => {
  return (
    <div>
      <header>
        <div className="container">
          <div className="container__main">
            <div className="container__left">
              <div className="nav__logo" style={{ fontWeight: "bold" }}>
                Todo
              </div>
            </div>
            <div className="container__middle">
              <ion-icon name="search-outline"></ion-icon>
              <input
                type="text"
                placeholder="Quick find"
                onChange={(e) => props.setSearchTerm(e.target.value)}
              />
            </div>
            <div
              className="container__right"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around",
              }}
            >
              <ion-icon name="add-outline"></ion-icon>
              <ion-icon name="contrast-outline"></ion-icon>
              <ion-icon name="notifications"></ion-icon>
              <ion-icon name="settings"></ion-icon>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
};

Header.propTypes = {
  setSearchTerm: PropTypes.func,
};

export default Header;
