import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { listTodo } from "./actions/todoActions";
import Loader from "./components/loader/Loader";
import TodoItem from "./components/todoItem/TodoItem";
import Header from "./components/header/Header";
import "./App.css";
import TodoDetail from "./components/tododetail/TodoDetail";
import TodoAction from "./components/todoaction/TodoAction";
import TodoMenu from "./components/todomenu/TodoMenu";

const App = () => {
  const [currentTodo, setCurrentTodo] = useState(null);
  const dispatch = useDispatch();
  const todoList = useSelector((state) => state.todoList);
  const { loading, error, todos } = todoList;

  const [searchTerm, setSearchTerm] = useState("");
  useEffect(() => {
    dispatch(listTodo());
  }, [dispatch]);
  return (
    <div className="container">
      <TodoMenu />
      <Header setSearchTerm={setSearchTerm} />
      <main className="container__main">
        <aside
          className="container__left"
          style={{
            backgroundColor: "#fafafa",
            overflowY: "scroll",
            height: "92vh",
          }}
        >
          {loading ? (
            <Loader />
          ) : error ? (
            <p>{error}</p>
          ) : (
            <div>
              {todos?.map((todo, index) => (
                <TodoItem
                  todo={todo}
                  key={index}
                  onClick={() => setCurrentTodo(todo)}
                />
              ))}
            </div>
          )}
        </aside>
        <article
          id="todo-detail"
          className="container__middle"
          style={{
            borderLeft: "2px solid #F0F0F0",
            borderRight: "2px solid #F0F0F0",
          }}
        >
          {currentTodo ? (
            <TodoDetail current={currentTodo} />
          ) : (
            <p>Click todo to see details</p>
          )}
        </article>
        <nav className="container__right">
          {currentTodo ? (
            <TodoAction current={currentTodo} />
          ) : (
            <p>Click todo to see action</p>
          )}
        </nav>
      </main>
      <footer
        style={{
          borderTop: "2px solid #F2F2F2",
        }}
      />
    </div>
  );
};

export default App;
