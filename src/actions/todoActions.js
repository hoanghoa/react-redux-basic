import Axios from "axios";
import {
  TODO_LIST_FAIL,
  TODO_LIST_REQUEST,
  TODO_LIST_SUCCESS,
} from "../constants/todoConstants";

export const listTodo = () => async (dispatch) => {
  dispatch({
    type: TODO_LIST_REQUEST,
  });

  try {
    const { data } = await Axios.get(
      "https://jsonplaceholder.typicode.com/todos"
    );
    dispatch({ type: TODO_LIST_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: TODO_LIST_FAIL, payload: error.message });
  }
};
